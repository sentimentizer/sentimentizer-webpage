# Readme

The Sentimentizer Webpage is the "front end" for the app. Users bring the page up on their browser and enter their vote - this will trigger a call to the Kinesis API for the DataPipelineDemo stream.

## Deployment Notes

The page is designed to run on an S3 Bucket configured for Static Webpages. You will need to upload both the  HTML page and the specially compiled AWS JavaScript SDK (in the repo) as it has been compiled with the Kinesis library. 

Credentials are hard-coded, and need to be locked down as follows:
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1388783177000",
      "Effect": "Allow",
      "Action": [
        "kinesis:PutRecord"
      ],
      "Resource": [
        "<arn>"
      ]
    }
  ]
}
```